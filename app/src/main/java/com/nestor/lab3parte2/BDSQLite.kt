package com.nestor.lab3parte2
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class BDSQLite(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "PersonasDB"
        private const val TABLE_NAME = "personas"
        private const val KEY_ID = "id"
        private const val KEY_NOMBRE = "nombre"
        private const val KEY_APELLIDO = "apellido"
        private const val KEY_TELEFONO = "telefono"
        private const val KEY_IMAGEN = "imagen"

    }
    //Método para obtener todas las personas de la base de datos
    // Método para obtener todas las personas de la base de datos
    fun getAllPersonas(): MutableList<Persona> {
        val personasList = mutableListOf<Persona>()
        // Agregar algunos registros de prueba manualmente
        personasList.add(
            Persona(
                1,
                "Waffle de Frambuesa",
                "Delicioso waffle crujiente cubierto con una generosa porción de crema batida y una exquisita salsa de frambuesa fresca.",
                "$8.99",
                "https://www.foodpal-app.com/uploads/images/meals/5231/sahnewaffeln-mit-himbeerquark-602396a7348cb-800.webp"
            )
        )
        personasList.add(
            Persona(
                2,
                "Waffle de Frambuesa",
                "Waffle recién horneado, suave por dentro y crujiente por fuera, adornado con una capa de crema batida y acompañado de una salsa de frambuesa casera.",
                "$10.50",
                "https://www.foodpal-app.com/uploads/images/meals/5231/sahnewaffeln-mit-himbeerquark-602396a7348cb-800.webp"
            )
        )
        //...

        // Resto del código de consulta aquí

        return personasList
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE $TABLE_NAME ($KEY_ID INTEGER PRIMARY KEY, $KEY_NOMBRE TEXT, $KEY_APELLIDO TEXT, $KEY_TELEFONO TEXT, $KEY_IMAGEN TEXT)"
        db?.execSQL(createTable)


    }



    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun addProduct(producto: Persona) {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(KEY_NOMBRE, producto.nombre)
            put(KEY_APELLIDO, producto.apellido)
            put(KEY_TELEFONO, producto.telefono)
            put(KEY_IMAGEN, producto.imagen)
        }
        db.insert(TABLE_NAME, null, values)
        db.close()
    }

    fun deleteProduct(producto: Persona) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "$KEY_ID = ?", arrayOf(producto.id.toString()))
        db.close()
    }

    // Otros métodos para actualizar, obtener y eliminar productos
}
