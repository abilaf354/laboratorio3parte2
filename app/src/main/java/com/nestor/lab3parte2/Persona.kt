package com.nestor.lab3parte2

data class Persona(
    var id: Int = 0, // Agregar la propiedad id
    var nombre: String? = null,
    var apellido: String? = null,
    var telefono: String? = null,
    var imagen: String? = null
) {
    constructor(id: Int, nombre: String, apellido: String, telefono: String) : this(id, nombre, apellido, telefono, null)
    override fun toString(): String {
        return nombre ?: ""
    }
}

