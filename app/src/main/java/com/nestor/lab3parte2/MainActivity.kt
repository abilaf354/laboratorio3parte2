package com.nestor.lab3parte2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val r = findViewById<RecyclerView>(R.id.recycler1)

        // Crear una instancia de DatabaseHandler
        val databaseHandler = BDSQLite(this)

        // Obtener la lista de personas desde la base de datos
        val personasList = databaseHandler.getAllPersonas()

        val personasAdapter = PersonasAdapter(applicationContext, personasList, databaseHandler)
        r.adapter = personasAdapter

        val layout = LinearLayoutManager(applicationContext)
        layout.orientation = LinearLayoutManager.VERTICAL
        r.layoutManager = layout
    }
}

