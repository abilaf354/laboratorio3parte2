package com.nestor.lab3parte2
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
class PersonasAdapter(
    private val context: Context,
private val personasModel: List<Persona>,
private val databaseHandler: BDSQLite // Añadir referencia a DatabaseHandler
) : RecyclerView.Adapter<PersonasAdapter.PersonasViewHolder>() {

    inner class PersonasViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageUrl: ImageView = itemView.findViewById(R.id.imageLogo)
        var titulo: TextView = itemView.findViewById(R.id.txtTitulo)
        var descripcion: TextView = itemView.findViewById(R.id.txtDescripcion)
        var precio: TextView = itemView.findViewById(R.id.txtprecio)
        var btnModificar: Button = itemView.findViewById(R.id.btnModificar)
        var btnEliminar: Button = itemView.findViewById(R.id.btnEliminar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonasViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_persona, parent, false)
        return PersonasViewHolder(v)
    }

    override fun getItemCount(): Int {
        return personasModel.size
    }

    override fun onBindViewHolder(holder: PersonasViewHolder, position: Int) {
        val persona = personasModel[position]
        holder.titulo.text = persona.nombre
        holder.descripcion.text = persona.apellido
        holder.precio.text = persona.telefono


        // Cargar imagen usando Glide (requiere implementación)
        Glide.with(context)
            .load(persona.imagen)
            .centerCrop()
            .into(holder.imageUrl)

        // Manejar clic en botón Modificar
        holder.btnModificar.setOnClickListener {
            // Implementar lógica para modificar la persona
            // Puedes abrir una actividad o diálogo de edición pasando la persona actual
            // holder.adapterPosition te dará la posición de la persona en la lista
        }

        // Manejar clic en botón Eliminar
        holder.btnEliminar.setOnClickListener {
            // Implementar lógica para eliminar la persona
            val personaAEliminar = personasModel[holder.adapterPosition]
            databaseHandler.deleteProduct(personaAEliminar) // Llamar al método deletePersona() de DatabaseHandler
            personasModel.toMutableList().removeAt(holder.adapterPosition) // Eliminar persona de la lista
            notifyDataSetChanged() // Notificar al adaptador que se eliminó un elemento
        }
    }
}